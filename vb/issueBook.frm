VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form issueBook 
   Appearance      =   0  'Flat
   BackColor       =   &H80000005&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   7365
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   17790
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   7365
   ScaleWidth      =   17790
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H80000008&
      Height          =   8655
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   17895
      Begin VB.Frame Frame2 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   0  'None
         Caption         =   "Book And Meember detail"
         ForeColor       =   &H80000008&
         Height          =   1455
         Left            =   5160
         TabIndex        =   11
         Top             =   120
         Width           =   4575
         Begin VB.Label issueDatePreview 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "This book will be issued on:"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   375
            Left            =   120
            TabIndex        =   14
            Top             =   120
            Width           =   4335
         End
         Begin VB.Label bookPreviewLabel 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Book Name:"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   375
            Left            =   120
            TabIndex        =   13
            Top             =   1080
            Visible         =   0   'False
            Width           =   4335
         End
         Begin VB.Label memberPreviewLabel 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Member Name: "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   375
            Left            =   120
            TabIndex        =   12
            Top             =   600
            Visible         =   0   'False
            Width           =   4335
         End
      End
      Begin MSDataGridLib.DataGrid transferTblGrid 
         Bindings        =   "issueBook.frx":0000
         Height          =   495
         Left            =   4440
         TabIndex        =   10
         Top             =   3720
         Visible         =   0   'False
         Width           =   1815
         _ExtentX        =   3201
         _ExtentY        =   873
         _Version        =   393216
         HeadLines       =   1
         RowHeight       =   15
         FormatLocked    =   -1  'True
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnCount     =   6
         BeginProperty Column00 
            DataField       =   "record_id"
            Caption         =   "record_id"
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
         EndProperty
         BeginProperty Column01 
            DataField       =   "member_id"
            Caption         =   "member_id"
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
         EndProperty
         BeginProperty Column02 
            DataField       =   "book_id"
            Caption         =   "book_id"
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
         EndProperty
         BeginProperty Column03 
            DataField       =   "transfer_date"
            Caption         =   "transfer_date"
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
         EndProperty
         BeginProperty Column04 
            DataField       =   "return_date"
            Caption         =   "return_date"
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
         EndProperty
         BeginProperty Column05 
            DataField       =   "is_returned"
            Caption         =   "is_returned"
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
         EndProperty
         SplitCount      =   1
         BeginProperty Split0 
            BeginProperty Column00 
               ColumnWidth     =   1739.906
            EndProperty
            BeginProperty Column01 
               ColumnWidth     =   1739.906
            EndProperty
            BeginProperty Column02 
               ColumnWidth     =   1739.906
            EndProperty
            BeginProperty Column03 
               ColumnWidth     =   1739.906
            EndProperty
            BeginProperty Column04 
               ColumnWidth     =   1739.906
            EndProperty
            BeginProperty Column05 
               ColumnWidth     =   794.835
            EndProperty
         EndProperty
      End
      Begin MSAdodcLib.Adodc transferAdodc 
         Height          =   450
         Left            =   4560
         Top             =   3120
         Visible         =   0   'False
         Width           =   2055
         _ExtentX        =   3625
         _ExtentY        =   794
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   2
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=database/LMSDB.accdb;Persist Security Info=False"
         OLEDBString     =   "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=database/LMSDB.accdb;Persist Security Info=False"
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   "transfer_records"
         Caption         =   "transferAdodc"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin MSDataGridLib.DataGrid memberTblArray 
         Bindings        =   "issueBook.frx":001C
         Height          =   495
         Left            =   2280
         TabIndex        =   9
         Top             =   3720
         Visible         =   0   'False
         Width           =   1935
         _ExtentX        =   3413
         _ExtentY        =   873
         _Version        =   393216
         HeadLines       =   1
         RowHeight       =   15
         FormatLocked    =   -1  'True
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnCount     =   6
         BeginProperty Column00 
            DataField       =   "member_id"
            Caption         =   "member_id"
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
         EndProperty
         BeginProperty Column01 
            DataField       =   "member_name"
            Caption         =   "member_name"
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
         EndProperty
         BeginProperty Column02 
            DataField       =   "address"
            Caption         =   "address"
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
         EndProperty
         BeginProperty Column03 
            DataField       =   "phone"
            Caption         =   "phone"
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
         EndProperty
         BeginProperty Column04 
            DataField       =   "register_date"
            Caption         =   "register_date"
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
         EndProperty
         BeginProperty Column05 
            DataField       =   "can_issue"
            Caption         =   "can_issue"
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
         EndProperty
         SplitCount      =   1
         BeginProperty Split0 
            BeginProperty Column00 
               ColumnWidth     =   1739.906
            EndProperty
            BeginProperty Column01 
               ColumnWidth     =   1739.906
            EndProperty
            BeginProperty Column02 
               ColumnWidth     =   1739.906
            EndProperty
            BeginProperty Column03 
               ColumnWidth     =   1739.906
            EndProperty
            BeginProperty Column04 
               ColumnWidth     =   1739.906
            EndProperty
            BeginProperty Column05 
               ColumnWidth     =   764.787
            EndProperty
         EndProperty
      End
      Begin MSAdodcLib.Adodc memberAdodc 
         Height          =   375
         Left            =   2520
         Top             =   3120
         Visible         =   0   'False
         Width           =   1935
         _ExtentX        =   3413
         _ExtentY        =   661
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   2
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=database/LMSDB.accdb;Persist Security Info=False"
         OLEDBString     =   "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=database/LMSDB.accdb;Persist Security Info=False"
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   "members"
         Caption         =   "memberAdodc"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin MSComCtl2.DTPicker rdateInput 
         Height          =   375
         Left            =   2760
         TabIndex        =   3
         Top             =   2520
         Visible         =   0   'False
         Width           =   2535
         _ExtentX        =   4471
         _ExtentY        =   661
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   16187393
         CurrentDate     =   42799
      End
      Begin VB.TextBox memberidInput 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   2280
         TabIndex        =   1
         Top             =   240
         Width           =   2535
      End
      Begin VB.TextBox bookidInput 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   2280
         TabIndex        =   2
         Top             =   840
         Width           =   2535
      End
      Begin VB.CommandButton addBtn 
         Caption         =   "Issue a Book"
         Height          =   615
         Left            =   360
         TabIndex        =   4
         Top             =   2280
         Width           =   2175
      End
      Begin MSAdodcLib.Adodc bookAdodc 
         Height          =   450
         Left            =   360
         Top             =   3120
         Visible         =   0   'False
         Width           =   1935
         _ExtentX        =   3413
         _ExtentY        =   794
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=database/LMSDB.accdb;Persist Security Info=False"
         OLEDBString     =   "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=database/LMSDB.accdb;Persist Security Info=False"
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "bookAdodc"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin MSDataGridLib.DataGrid bookTblGrid 
         Height          =   495
         Left            =   360
         TabIndex        =   5
         Top             =   3720
         Visible         =   0   'False
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   873
         _Version        =   393216
         HeadLines       =   1
         RowHeight       =   15
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnCount     =   2
         BeginProperty Column00 
            DataField       =   ""
            Caption         =   ""
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
         EndProperty
         BeginProperty Column01 
            DataField       =   ""
            Caption         =   ""
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
         EndProperty
         SplitCount      =   1
         BeginProperty Split0 
            BeginProperty Column00 
            EndProperty
            BeginProperty Column01 
            EndProperty
         EndProperty
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00C0C0C0&
         X1              =   5040
         X2              =   5040
         Y1              =   120
         Y2              =   1920
      End
      Begin VB.Label returnDatePreview 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "Return Date"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   2280
         TabIndex        =   15
         Top             =   1440
         Width           =   1695
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "Member ID"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   360
         TabIndex        =   8
         Top             =   240
         Width           =   1215
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "Book ID"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   360
         TabIndex        =   7
         Top             =   840
         Width           =   1335
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "Return Date"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   360
         TabIndex        =   6
         Top             =   1440
         Width           =   1695
      End
   End
End
Attribute VB_Name = "issueBook"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public bookIdtoIssue, memberIdtoIssue As String

Private Sub Form_Load()
    header.title.Caption = "Issue a Book"
    header.desc.Caption = "Issue an available book to the user"
    
    If (Val(bookIdtoIssue) <> 0) Then
    bookidInput.Text = bookIdtoIssue
    End If
    
    If (Val(memberIdtoIssue) <> 0) Then
    memberidInput.Text = bookIdtoIssue
    End If
    
    'add 15 days more the return date
    rdateInput.Value = DateValue(Now) + 15
    returnDatePreview.Caption = rdateInput.Value
    
    issueDatePreview.Caption = "Book will be issued on: " + Str(DateValue(Now))
End Sub

Private Sub addBtn_Click()
    'Error handling
    If (Val(memberidInput.Text) = 0) Then
        MsgBox "Member ID is not valid!", vbOKOnly, "ERROR"
        Exit Sub
    End If

    If (Val(bookidInput.Text) = 0) Then
        MsgBox "Book ID is not valid!", vbOKOnly, "ERROR"
        Exit Sub
    End If
    
    If (rdateInput.Value <= DateValue(Now)) Then
        MsgBox "Return date can't be today or earlier", vbOKOnly, "ERROR"
        Exit Sub
    End If
    
    'Check member ID exists on the database or not!
    'also make sure if member does not have any previous issued books waiting to be returned
    memberAdodc.Refresh
    memberAdodc.Recordset.Find "member_id=" + Str(Val(memberidInput.Text))
        
    If (memberAdodc.Recordset.EOF = False) Then
        With memberAdodc.Recordset
            If (.Fields(5) = "False") Then
                MsgBox "This member have already issued books, No more can be issued until the previous book is returned", vbOKOnly, "ERROR"
                Exit Sub
            End If
        End With
    Else
        MsgBox "We can't find Member ID on the database!", vbOKOnly, "ERROR"
        Exit Sub
    End If
    
    'Check book ID exists on the database or not!
    'Also check if the book is issued to another member or not
    bookAdodc.CommandType = adCmdText
    bookAdodc.RecordSource = "Select * from books where book_id='" + bookidInput.Text + "'"
    bookAdodc.Refresh
    Set bookTblGrid.DataSource = bookAdodc

    If (bookAdodc.Recordset.EOF = False) Then
        With bookAdodc.Recordset
            If (.Fields(5) = "False") Then
                MsgBox "The Book is already issued to another member!", vbOKOnly, "ERROR"
                Exit Sub
            End If
        End With
    Else
        MsgBox "The Book is not available!", vbOKOnly, "ERROR"
        Exit Sub
    End If

    'Ok everything seems right so far, save to database!
    With memberAdodc.Recordset
        .Fields(5) = "0"
        .Update
    End With
    
    With bookAdodc.Recordset
        .Fields(5) = "0"
        .Update
    End With
    
    'And finally add the transfer record
    'last transfer record id so that we can increment it
    Dim lastRecordId As String

    '@todo, add input validation
    'increment the record id by 1 if there are already record exists
    If (transferAdodc.Recordset.RecordCount > 0) Then
        With transferAdodc.Recordset
            .MoveLast
            lastRecordId = .Fields(0)
        End With
        lastRecordId = Trim(Str(Val(lastRecordId) + 1))
    Else
        lastRecordId = Trim(Str(1))
    End If
    
    'Insert a new record to the database
    With transferAdodc.Recordset
        .AddNew
        .Fields(0) = lastRecordId
        .Fields(1) = memberidInput.Text
        .Fields(2) = bookidInput.Text
        .Fields(3) = DateValue(Now)
        .Fields(4) = rdateInput.Value
        .Fields(5) = "0"
    End With
    
    memberAdodc.Refresh
    bookAdodc.Refresh

    
    MsgBox "Book has been successfully issued", vbOKOnly, "Book is now issued"
    clearField
End Sub

Private Sub clearField()
    bookIdtoIssue = ""
    memberIdtoIssue = ""
    memberidInput.Text = ""
    bookidInput.Text = ""
End Sub


Private Sub Form_Unload(Cancel As Integer)
    clearField
End Sub

'Get member preview
Private Sub memberidInput_LostFocus()
    If (memberidInput.Text = "") Then
        Exit Sub
    End If
    'Check member ID exists on the database or not!
    'also make sure if member does not have any previous issued books waiting to be returned
    memberAdodc.Refresh
    memberAdodc.Recordset.Find "member_id=" + Str(Val(memberidInput.Text))
        
    If (memberAdodc.Recordset.EOF = False) Then
        With memberAdodc.Recordset
            If (.Fields(5) = "False") Then
                MsgBox "This member have already issued books, No more can be issued until the previous book is returned", vbOKOnly, "ERROR"
                Exit Sub
            End If
            
            'Since the user can be issued show a preview
            memberPreviewLabel.Caption = "Member Name: " + .Fields(1)
        End With
    Else
        MsgBox "We can't find Member ID on the database!", vbOKOnly, "ERROR"
        Exit Sub
    End If
    
    memberPreviewLabel.Visible = True
End Sub

Private Sub bookidInput_LostFocus()
    If (bookidInput.Text = "") Then
        Exit Sub
    End If

    'Check book ID exists on the database or not!
    'Also check if the book is issued to another member or not
    bookAdodc.CommandType = adCmdText
    bookAdodc.RecordSource = "Select * from books where book_id='" + bookidInput.Text + "'"
    bookAdodc.Refresh
    Set bookTblGrid.DataSource = bookAdodc

    If (bookAdodc.Recordset.EOF = False) Then
        With bookAdodc.Recordset
            If (.Fields(5) = "False") Then
                MsgBox "The Book is already issued to another member!", vbOKOnly, "ERROR"
                Exit Sub
            End If
            bookPreviewLabel.Caption = "Book Name: " + .Fields(1)
        End With
    Else
        MsgBox "The Book is not available!", vbOKOnly, "ERROR"
        Exit Sub
    End If
    bookPreviewLabel.Visible = True
End Sub

VERSION 5.00
Begin VB.Form sidebar 
   BackColor       =   &H00404040&
   BorderStyle     =   0  'None
   Caption         =   "sidebar"
   ClientHeight    =   7440
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   3720
   FillColor       =   &H00FFFFFF&
   ForeColor       =   &H80000014&
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   7440
   ScaleWidth      =   3720
   ShowInTaskbar   =   0   'False
   Begin VB.Frame memberFrame 
      Appearance      =   0  'Flat
      BackColor       =   &H00404040&
      BorderStyle     =   0  'None
      Caption         =   "book"
      ForeColor       =   &H80000008&
      Height          =   1935
      Left            =   0
      TabIndex        =   6
      Top             =   4200
      Width           =   3720
      Begin VB.PictureBox sbarBtn4 
         Appearance      =   0  'Flat
         BackColor       =   &H00000000&
         BorderStyle     =   0  'None
         ForeColor       =   &H8000000E&
         Height          =   615
         Left            =   0
         ScaleHeight     =   615
         ScaleWidth      =   3780
         TabIndex        =   9
         Top             =   720
         Width           =   3780
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Add new member"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000014&
            Height          =   240
            Left            =   270
            TabIndex        =   10
            Top             =   225
            Width           =   1515
         End
      End
      Begin VB.PictureBox sbarBtn5 
         Appearance      =   0  'Flat
         BackColor       =   &H00000000&
         BorderStyle     =   0  'None
         ForeColor       =   &H8000000E&
         Height          =   615
         Left            =   0
         ScaleHeight     =   615
         ScaleWidth      =   3780
         TabIndex        =   7
         Top             =   1320
         Width           =   3780
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "View member data"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000014&
            Height          =   240
            Left            =   270
            TabIndex        =   8
            Top             =   225
            Width           =   1620
         End
      End
      Begin VB.Label Label7 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Member"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   255
         Left            =   240
         TabIndex        =   11
         Top             =   240
         Width           =   1575
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00000000&
         BackStyle       =   1  'Opaque
         BorderStyle     =   0  'Transparent
         Height          =   735
         Left            =   0
         Top             =   0
         Width           =   3975
      End
   End
   Begin VB.Frame bookFrame 
      Appearance      =   0  'Flat
      BackColor       =   &H00404040&
      BorderStyle     =   0  'None
      Caption         =   "book"
      ForeColor       =   &H80000008&
      Height          =   3255
      Left            =   -15
      TabIndex        =   0
      Top             =   1080
      Width           =   3720
      Begin VB.PictureBox sbarBtn8 
         Appearance      =   0  'Flat
         BackColor       =   &H00000000&
         BorderStyle     =   0  'None
         ForeColor       =   &H8000000E&
         Height          =   615
         Left            =   0
         ScaleHeight     =   615
         ScaleWidth      =   3780
         TabIndex        =   14
         Top             =   2520
         Width           =   3780
         Begin VB.Label Label9 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Submit Book return"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000014&
            Height          =   240
            Left            =   270
            TabIndex        =   15
            Top             =   225
            Width           =   1680
         End
      End
      Begin VB.PictureBox sbarBtn7 
         Appearance      =   0  'Flat
         BackColor       =   &H00000000&
         BorderStyle     =   0  'None
         ForeColor       =   &H8000000E&
         Height          =   615
         Left            =   0
         ScaleHeight     =   615
         ScaleWidth      =   3780
         TabIndex        =   12
         Top             =   1920
         Width           =   3780
         Begin VB.Label Label8 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Issue a Book"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000014&
            Height          =   240
            Left            =   270
            TabIndex        =   13
            Top             =   225
            Width           =   1140
         End
      End
      Begin VB.PictureBox sbarBtn2 
         Appearance      =   0  'Flat
         BackColor       =   &H00000000&
         BorderStyle     =   0  'None
         ForeColor       =   &H8000000E&
         Height          =   615
         Left            =   0
         ScaleHeight     =   615
         ScaleWidth      =   3780
         TabIndex        =   4
         Top             =   1320
         Width           =   3780
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "View book entry"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000014&
            Height          =   240
            Left            =   270
            TabIndex        =   5
            Top             =   225
            Width           =   1395
         End
      End
      Begin VB.PictureBox sbarBtn1 
         Appearance      =   0  'Flat
         BackColor       =   &H00000000&
         BorderStyle     =   0  'None
         ForeColor       =   &H8000000E&
         Height          =   615
         Left            =   0
         MousePointer    =   99  'Custom
         ScaleHeight     =   615
         ScaleWidth      =   3780
         TabIndex        =   1
         Top             =   720
         Width           =   3780
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Add new entry"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000014&
            Height          =   240
            Left            =   270
            TabIndex        =   2
            Top             =   225
            Width           =   1245
         End
      End
      Begin VB.Label Label4 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Book"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000014&
         Height          =   285
         Left            =   180
         TabIndex        =   3
         Top             =   240
         Width           =   1095
      End
      Begin VB.Shape Shape2 
         BackColor       =   &H00000000&
         BackStyle       =   1  'Opaque
         BorderStyle     =   0  'Transparent
         Height          =   735
         Left            =   0
         Top             =   0
         Width           =   3975
      End
   End
   Begin VB.Image Image1 
      Height          =   1110
      Left            =   0
      Picture         =   "Sidebar.frx":0000
      Top             =   0
      Width           =   3750
   End
End
Attribute VB_Name = "sidebar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim sbarBtn1Hover, sbarBtn2Hover, sbarBtn4Hover, sbarBtn5Hover, sbarBtn7Hover, sbarBtn8Hover As Boolean

Private Sub Form_Load()
    resetSidebarColor
End Sub

Private Sub resetSidebarColor()
    resetSidebarHover
    bookFrame.BackColor = BaseForm.sidebarBack
    memberFrame.BackColor = BaseForm.sidebarBack
    Shape1.BackColor = BaseForm.sidebarhedaer
    Shape2.BackColor = BaseForm.sidebarhedaer
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    resetSidebarHover
End Sub

Private Sub BookFrame_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    resetSidebarHover
End Sub

Private Sub memberFrame_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    resetSidebarHover
End Sub

Private Sub resetSidebarHover()
    sbarBtn1Hover = False
    removeHoverEffect sbarBtn1
    sbarBtn2Hover = False
    removeHoverEffect sbarBtn2
    sbarBtn4Hover = False
    removeHoverEffect sbarBtn4
    sbarBtn5Hover = False
    removeHoverEffect sbarBtn5
    sbarBtn7Hover = False
    removeHoverEffect sbarBtn7
    sbarBtn8Hover = False
    removeHoverEffect sbarBtn8
End Sub

Private Sub addHoverEffect(btn As PictureBox)
    btn.BackColor = RGB(42, 42, 42)
End Sub

Private Sub removeHoverEffect(btn As PictureBox)
    btn.BackColor = BaseForm.sidebarBack
End Sub

Private Sub sbarBtn1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
If (sbarBtn1Hover = False) Then
    resetSidebarHover
    addHoverEffect sbarBtn1
    sbarBtn1Hover = True
End If
End Sub


Private Sub sbarBtn2_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
If sbarBtn2Hover = False Then
    resetSidebarHover
    addHoverEffect sbarBtn2
    sbarBtn2Hover = True
End If
End Sub

Private Sub sbarBtn4_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
If sbarBtn4Hover = False Then
    resetSidebarHover
    addHoverEffect sbarBtn4
    sbarBtn4Hover = True
End If
End Sub

Private Sub sbarBtn5_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
If sbarBtn5Hover = False Then
    resetSidebarHover
    addHoverEffect sbarBtn5
    sbarBtn5Hover = True
End If
End Sub

Private Sub sbarBtn7_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
If sbarBtn7Hover = False Then
    resetSidebarHover
    addHoverEffect sbarBtn7
    sbarBtn7Hover = True
End If
End Sub

Private Sub sbarBtn8_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
If sbarBtn8Hover = False Then
    resetSidebarHover
    addHoverEffect sbarBtn8
    sbarBtn8Hover = True
End If
End Sub


Public Sub sbarBtn1_Click()
    BaseForm.showForm addBookEntry
End Sub

Public Sub sbarBtn2_Click()
    BaseForm.showForm viewBookEntry
End Sub

Public Sub sbarBtn4_Click()
    BaseForm.showForm addMemberEntry
End Sub

Public Sub sbarBtn5_Click()
    BaseForm.showForm viewMemberEntry
End Sub

Public Sub sbarBtn7_Click()
    BaseForm.showForm issueBook
End Sub

Public Sub sbarBtn8_Click()
    BaseForm.showForm returnBook
End Sub


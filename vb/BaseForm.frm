VERSION 5.00
Begin VB.MDIForm BaseForm 
   Appearance      =   0  'Flat
   BackColor       =   &H8000000C&
   Caption         =   "Library Management System"
   ClientHeight    =   10035
   ClientLeft      =   165
   ClientTop       =   735
   ClientWidth     =   17655
   Icon            =   "BaseForm.frx":0000
   LinkTopic       =   "MDIForm1"
   StartUpPosition =   3  'Windows Default
   Begin VB.Menu mnuBook 
      Caption         =   "Book"
      NegotiatePosition=   1  'Left
      Begin VB.Menu mnuBooknewentry 
         Caption         =   "Add new entry"
         Shortcut        =   ^N
      End
      Begin VB.Menu mnuBookBiew 
         Caption         =   "View Book Entry"
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuBookIssue 
         Caption         =   "Issue a Book"
         Shortcut        =   ^I
      End
      Begin VB.Menu mnuBookReturn 
         Caption         =   "Return a book"
         Shortcut        =   ^R
      End
   End
   Begin VB.Menu mnuMember 
      Caption         =   "Member"
      NegotiatePosition=   1  'Left
      Begin VB.Menu menuMemberAdd 
         Caption         =   "Add a new member"
         Shortcut        =   ^M
      End
      Begin VB.Menu mnuMemberView 
         Caption         =   "View Member data"
         Shortcut        =   ^D
      End
   End
   Begin VB.Menu mnuReport 
      Caption         =   "Report Wizard"
      NegotiatePosition=   1  'Left
      Begin VB.Menu mnuReportGenerateMember 
         Caption         =   "Generate Member report"
      End
      Begin VB.Menu mnuReportGenerateBook 
         Caption         =   "Generate Book Report"
      End
      Begin VB.Menu mnuReportGenerateDatewiseBookIssue 
         Caption         =   "Generate Date wise Book Issue"
      End
      Begin VB.Menu mnuReportGenerateDatewiseBookSubmit 
         Caption         =   "Generate Date wise Book Submit"
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "Help"
      NegotiatePosition=   1  'Left
   End
   Begin VB.Menu mnuLogout 
      Caption         =   "Logout"
      NegotiatePosition=   3  'Right
   End
End
Attribute VB_Name = "BaseForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim isLoggedIn As Boolean
Public sidebarWidth As Integer
Public sidebarBack, sidebarhedaer As ColorConstants
Public currentActiveForm As Form

Private Sub MDIForm_Load()
    'predefined width and styling
    sidebarBack = RGB(19, 19, 19)
    sidebarhedaer = RGB(30, 30, 30)
    BaseForm.BackColor = RGB(217, 217, 217)
    sidebar.BackColor = sidebarBack
    sidebarRepos
    If isLoggedIn = False Then
        Set currentActiveForm = Login
        formRepos currentActiveForm
    End If
    headerRepos
    toggleMnuOnLogin
    
    'add copyright and other info
    BaseForm.Caption = BaseForm.Caption + " - by AvikB"
End Sub

'Reszie and reposition system
Private Sub MDIForm_Resize()
    On Error Resume Next
    sidebarRepos
    formRepos currentActiveForm
    headerRepos
    minimumSize
End Sub

Private Sub minimumSize()
If (BaseForm.Width < 14000 Or BaseForm.Height < 10000) Then
BaseForm.Width = 15000
BaseForm.Height = 10000
End If
End Sub

Private Sub sidebarRepos()
    sidebarWidth = sidebar.Width
    sidebar.Height = BaseForm.ScaleHeight
End Sub

Private Sub formRepos(frm As Form)
    frm.Left = sidebarWidth
    frm.Top = header.Height
    frm.Width = BaseForm.ScaleWidth - sidebarWidth
    frm.Height = BaseForm.ScaleHeight - header.Height
End Sub

Private Sub headerRepos()
    header.Left = sidebarWidth
    header.Top = 0
    header.Width = BaseForm.ScaleWidth - sidebarWidth
End Sub

'Remove all form when laoding a new one
Public Sub clearFormView()
    Unload currentActiveForm
    Set currentActiveForm = home
End Sub

Private Sub menuMemberAdd_Click()
    sidebar.sbarBtn4_Click
End Sub

Private Sub mnuBookBiew_Click()
    sidebar.sbarBtn2_Click
End Sub

Private Sub mnuBookIssue_Click()
    sidebar.sbarBtn7_Click
End Sub

Private Sub mnuBooknewentry_Click()
    sidebar.sbarBtn1_Click
End Sub

Private Sub mnuBookReturn_Click()
    sidebar.sbarBtn8_Click
End Sub

Private Sub mnuHelp_Click()
    help.Show
End Sub

'Menu Button click events
Private Sub mnuLogout_Click()
    doLogout
End Sub

'Admin authentication and logout
Public Sub doLogin()
    isLoggedIn = True
    clearFormView
    formRepos currentActiveForm
    headerRepos
    sidebar.Enabled = True
    toggleMnuOnLogin
End Sub

Public Sub doLogout()
    isLoggedIn = False
    showForm Login
    sidebar.Enabled = False
    toggleMnuOnLogin
End Sub

Public Sub toggleMnuOnLogin()
    If (isLoggedIn = True) Then
        mnuLogout.Visible = True
        mnuBook.Enabled = True
        mnuMember.Enabled = True
        mnuReport.Enabled = True
    Else
        mnuLogout.Visible = False
        mnuBook.Enabled = False
        mnuMember.Enabled = False
        mnuReport.Enabled = False
    End If
End Sub

Public Sub showForm(frm As Form)
    clearFormView
    Set currentActiveForm = frm
    formRepos currentActiveForm
    headerRepos
End Sub

Private Sub mnuMemberView_Click()
    sidebar.sbarBtn5_Click
End Sub

Private Sub mnuReportGenerateBook_Click()
    BookReport.Show
End Sub

Private Sub mnuReportGenerateDatewiseBookIssue_Click()
    DatewiseIssueReport.Show
End Sub

Private Sub mnuReportGenerateDatewiseBookSubmit_Click()
    DatewiseSubmitReport.Show
End Sub

Private Sub mnuReportGenerateMember_Click()
    MemberReport.Show
End Sub


VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form viewBookEntry 
   Appearance      =   0  'Flat
   BackColor       =   &H80000005&
   BorderStyle     =   0  'None
   Caption         =   "View book entry"
   ClientHeight    =   12390
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   14085
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   12390
   ScaleWidth      =   14085
   ShowInTaskbar   =   0   'False
   Begin VB.Frame msgFrame 
      Appearance      =   0  'Flat
      BackColor       =   &H000000C0&
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      ForeColor       =   &H80000008&
      Height          =   735
      Left            =   0
      TabIndex        =   6
      Top             =   1560
      Visible         =   0   'False
      Width           =   12495
      Begin VB.Timer msgHideTimer 
         Left            =   11880
         Top             =   120
      End
      Begin VB.Label msgLabel 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Error data shows here!"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   360
         TabIndex        =   7
         Top             =   240
         Width           =   1950
      End
   End
   Begin VB.Frame listViewFrame 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H80000008&
      Height          =   4095
      Left            =   0
      TabIndex        =   20
      Top             =   6720
      Visible         =   0   'False
      Width           =   12615
      Begin MSDataGridLib.DataGrid listGrid 
         Bindings        =   "viewBookEntry.frx":0000
         Height          =   3735
         Left            =   0
         TabIndex        =   21
         Top             =   0
         Width           =   12615
         _ExtentX        =   22251
         _ExtentY        =   6588
         _Version        =   393216
         AllowUpdate     =   -1  'True
         Appearance      =   0
         BackColor       =   16777215
         BorderStyle     =   0
         ForeColor       =   3355443
         HeadLines       =   2
         RowHeight       =   30
         RowDividerStyle =   6
         FormatLocked    =   -1  'True
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnCount     =   6
         BeginProperty Column00 
            DataField       =   "book_id"
            Caption         =   "ID"
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
         EndProperty
         BeginProperty Column01 
            DataField       =   "book_name"
            Caption         =   "Book Name"
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
         EndProperty
         BeginProperty Column02 
            DataField       =   "book_author"
            Caption         =   "Author"
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
         EndProperty
         BeginProperty Column03 
            DataField       =   "book_price"
            Caption         =   "Price"
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
         EndProperty
         BeginProperty Column04 
            DataField       =   "book_pdate"
            Caption         =   "Purchase Date"
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
         EndProperty
         BeginProperty Column05 
            DataField       =   "status"
            Caption         =   "Status"
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
         EndProperty
         SplitCount      =   1
         BeginProperty Split0 
            BeginProperty Column00 
               Alignment       =   3
               DividerStyle    =   6
               Locked          =   -1  'True
               ColumnWidth     =   929.764
            EndProperty
            BeginProperty Column01 
               DividerStyle    =   0
               ColumnWidth     =   3344.882
            EndProperty
            BeginProperty Column02 
               DividerStyle    =   0
               ColumnWidth     =   2970.142
            EndProperty
            BeginProperty Column03 
               DividerStyle    =   0
               ColumnWidth     =   1739.906
            EndProperty
            BeginProperty Column04 
               DividerStyle    =   6
               ColumnWidth     =   1739.906
            EndProperty
            BeginProperty Column05 
               Alignment       =   2
               ColumnWidth     =   764.787
            EndProperty
         EndProperty
      End
   End
   Begin VB.Frame bookdetailFrame 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      Caption         =   "detailFrame"
      ForeColor       =   &H80000008&
      Height          =   4935
      Left            =   120
      TabIndex        =   8
      Top             =   1680
      Width           =   12375
      Begin VB.CommandButton issuReturnBtn 
         Caption         =   "Issue this Book"
         Height          =   615
         Left            =   4320
         TabIndex        =   17
         Top             =   3960
         Width           =   2175
      End
      Begin VB.CommandButton delBtn 
         Caption         =   "Delete"
         Height          =   615
         Left            =   2640
         TabIndex        =   16
         Top             =   3960
         Width           =   1575
      End
      Begin VB.CommandButton editBtn 
         Caption         =   "Edit/Update Book detail"
         Height          =   615
         Left            =   240
         TabIndex        =   15
         Top             =   3960
         Width           =   2295
      End
      Begin VB.Frame borrowedInfo 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         ForeColor       =   &H80000008&
         Height          =   735
         Left            =   3360
         TabIndex        =   13
         Top             =   2880
         Width           =   5775
         Begin VB.Label borrowedLabel 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "This book is borrowed and can not be issued until it is returned!"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00404040&
            Height          =   495
            Left            =   240
            TabIndex        =   14
            Top             =   120
            Width           =   5415
            WordWrap        =   -1  'True
         End
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00C0C0C0&
         X1              =   3360
         X2              =   9120
         Y1              =   1800
         Y2              =   1800
      End
      Begin VB.Label pricelabel 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "price"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00808080&
         Height          =   255
         Left            =   3360
         TabIndex        =   12
         Top             =   2040
         Width           =   6015
      End
      Begin VB.Label publishdate 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "publishdate"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   255
         Left            =   3360
         TabIndex        =   11
         Top             =   1320
         Width           =   5895
      End
      Begin VB.Label authorname 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Author Name"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00808080&
         Height          =   255
         Left            =   3360
         TabIndex        =   10
         Top             =   960
         Width           =   5895
      End
      Begin VB.Image coverImg 
         Appearance      =   0  'Flat
         BorderStyle     =   1  'Fixed Single
         Height          =   3255
         Left            =   240
         Picture         =   "viewBookEntry.frx":0015
         Stretch         =   -1  'True
         Top             =   360
         Width           =   2595
      End
      Begin VB.Label bookname 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Book Name"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   495
         Left            =   3360
         TabIndex        =   9
         Top             =   480
         Width           =   5895
      End
   End
   Begin MSAdodcLib.Adodc Adodc1 
      Height          =   375
      Left            =   0
      Top             =   11400
      Visible         =   0   'False
      Width           =   2655
      _ExtentX        =   4683
      _ExtentY        =   661
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   2
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=database/LMSDB.accdb;Persist Security Info=False"
      OLEDBString     =   "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=database/LMSDB.accdb;Persist Security Info=False"
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   "books"
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Frame searchFrame 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H80000008&
      Height          =   1575
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   12495
      Begin VB.ComboBox viewCombo 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         IntegralHeight  =   0   'False
         ItemData        =   "viewBookEntry.frx":512E
         Left            =   7440
         List            =   "viewBookEntry.frx":5138
         TabIndex        =   18
         Text            =   "Single"
         Top             =   720
         Width           =   2055
      End
      Begin VB.CommandButton getBookDetailBtn 
         Caption         =   "Get book detail"
         Height          =   375
         Left            =   5280
         TabIndex        =   5
         Top             =   720
         Width           =   1815
      End
      Begin VB.TextBox searchInput 
         Appearance      =   0  'Flat
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3000
         TabIndex        =   4
         Top             =   720
         Width           =   1935
      End
      Begin VB.ComboBox searchType 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         IntegralHeight  =   0   'False
         ItemData        =   "viewBookEntry.frx":514A
         Left            =   360
         List            =   "viewBookEntry.frx":5154
         TabIndex        =   1
         Text            =   "searchType"
         Top             =   720
         Width           =   2415
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "View"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   7440
         TabIndex        =   19
         Top             =   360
         Width           =   2055
      End
      Begin VB.Label searchInputLabel 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Enter the book ID"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   3000
         TabIndex        =   3
         Top             =   360
         Width           =   4215
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Get book detail using:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   360
         TabIndex        =   2
         Top             =   360
         Width           =   2415
      End
   End
End
Attribute VB_Name = "viewBookEntry"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public Enum SearchTypeEnum
    book_id
    ISBN
End Enum

Public Enum MessageType
    Err
    Success
    Warning
End Enum

Dim isBookIssued As Boolean
Dim CurrentBookId As String
Public UpdatedBookId As String


Private Sub Form_Load()
    header.title.Caption = "Get Book detail"
    header.desc.Caption = "View book information using book id or ISBN"
    
    listViewFrame.BackColor = RGB(240, 240, 240)
    searchType.ListIndex = 0
    msgFrame.Visible = False
    bookdetailFrame.Visible = False
    listViewFrame.Visible = False
    
    ResizeListGrid
    Form_Resize
    
    
    'this basically preload a view if the book is updated and sent here
    If UpdatedBookId <> "" Then
        Adodc1.Refresh
        Adodc1.Recordset.Find "book_id=" + Str(Val(UpdatedBookId))
        
        If (Adodc1.Recordset.EOF = False) Then
            'record found show, show it to user
            With Adodc1.Recordset
                createBookDetailView .Fields(0), .Fields(1), .Fields(2), .Fields(3), .Fields(4), .Fields(5)
                searchInput.Text = UpdatedBookId
                showMsg "This book has been updated!", Success
            End With
        Else
            'no record found! ;(
            showMsg "We couldn't found this book in our library!", Err
        End If
    End If
End Sub

Private Sub Form_Resize()
    searchFrame.Width = Me.Width
    msgFrame.Width = Me.Width
    ResizeListGrid
    
    If (viewCombo.Text = "List") Then
        ResizeListGrid
    End If
End Sub

Private Sub ResizeListGrid()
    listGrid.Width = Me.Width
    listGrid.Height = Me.Height - searchFrame.Height
    listViewFrame.Width = Me.Width
    listViewFrame.Height = Me.Height
End Sub

Private Sub Form_Unload(Cancel As Integer)
    CurrentBookId = ""
    UpdatedBookId = ""
End Sub



Private Sub searchType_Click()
    Select Case searchType.ListIndex
        Case SearchTypeEnum.book_id
            searchInputLabel.Caption = "Enter the book ID"
        Case SearchTypeEnum.ISBN
            searchInputLabel.Caption = "Enter book ISBN"
    End Select
End Sub

Private Sub getBookDetailBtn_Click()
    Dim book_name As String
    
    'hide any previous error that is still showing!
    hideError
    
    'if no search string is provided then don't do anything
    If (searchInput.Text = "") Then
        showMsg "Searchbox is empty! please provide some input", Warning
        Exit Sub
    End If

    Adodc1.Refresh
    Adodc1.Recordset.Find "book_id=" + Str(Val(searchInput.Text))
    
    If (Adodc1.Recordset.EOF = False) Then
        'record found show, show it to user
        With Adodc1.Recordset
            createBookDetailView .Fields(0), .Fields(1), .Fields(2), .Fields(3), .Fields(4), .Fields(5)
        End With
    Else
        'no record found! ;(
        showMsg "We couldn't found this book in our library!", Err
    End If
End Sub


Private Sub createBookDetailView(book_id As String, book_name As String, author_name As String, price As String, pdate As String, status As String)
    CurrentBookId = book_id
    bookname.Caption = book_name
    authorname.Caption = author_name
    publishdate.Caption = "Purchased on: " + pdate
    pricelabel.Caption = "Price: " + price + "rs"
    If (status = "False") Then
        isBookIssued = True
        borrowedInfo.Visible = True
        delBtn.Enabled = False
        'issuReturnBtn.Caption = "Return this book"
        issuReturnBtn.Enabled = False
    Else
        isBookIssued = False
        borrowedInfo.Visible = False
        delBtn.Enabled = True
        issuReturnBtn.Caption = "Issue this book"
    End If
    
    If (viewCombo.Text = "Single") Then
        bookdetailFrame.Visible = True
    End If
End Sub

Private Sub issuReturnBtn_Click()
    If (isBookIssued) Then
        MsgBox "Book is issued, return this!"
    Else
        issueBook.bookIdtoIssue = CurrentBookId
        BaseForm.showForm issueBook
    End If
End Sub


Private Sub showMsg(msg As String, Optional mtype As MessageType)
    Select Case mtype
        Case MessageType.Err
        msgFrame.BackColor = RGB(192, 0, 0)
        
        Case MessageType.Success
        msgFrame.BackColor = RGB(104, 188, 62)
        
        Case MessageType.Warning
        msgFrame.BackColor = RGB(243, 127, 9)
    End Select
    
    msgFrame.Visible = True
    msgLabel.Caption = msg
    
    msgHideTimer.Enabled = False
    msgHideTimer.Interval = 3000
    msgHideTimer.Enabled = True
    
    bookdetailFrame.Top = 2400
End Sub

Private Sub hideError()
    msgFrame.Visible = False
    msgHideTimer.Enabled = False
    bookdetailFrame.Top = 1680
End Sub

'hides the error after some defined time
Private Sub msgHideTimer_Timer()
    hideError
End Sub


Private Sub delBtn_Click()
    Dim delConfirm As VbMsgBoxResult
    delConfirm = MsgBox("Are you sure you want to delete this book? This action is permanent!", vbYesNo, "Delete Book?")

    If delConfirm = vbYes Then
        On Error Resume Next
        Adodc1.Recordset.Delete
        CurrentBookId = Null
        isBookIssued = Null
        bookdetailFrame.Visible = False
        showMsg "Book is successfully deleted from your Library!", Success
    End If
End Sub



Private Sub editBtn_Click()
    editBookEntry.bookRecordIdtoModify = CurrentBookId
    BaseForm.showForm editBookEntry
End Sub


Private Sub viewCombo_Click()
    If (viewCombo.Text = "Single") Then
        If (CurrentBookId <> "") Then
            bookdetailFrame.Visible = True
        End If
        listViewFrame.Visible = False
    ElseIf (viewCombo.Text = "List") Then
        bookdetailFrame.Visible = False
        listViewFrame.Visible = True
        listViewFrame.Top = 1560
        listViewFrame.Left = 0
    End If
End Sub

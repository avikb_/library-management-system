VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form viewMemberEntry 
   Appearance      =   0  'Flat
   BackColor       =   &H80000005&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   11850
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   14940
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   11850
   ScaleWidth      =   14940
   ShowInTaskbar   =   0   'False
   Begin MSDataGridLib.DataGrid DataGrid1 
      Bindings        =   "viewMemberEntry.frx":0000
      Height          =   3735
      Left            =   120
      TabIndex        =   16
      Top             =   8160
      Visible         =   0   'False
      Width           =   12615
      _ExtentX        =   22251
      _ExtentY        =   6588
      _Version        =   393216
      AllowUpdate     =   -1  'True
      Appearance      =   0
      BackColor       =   16777215
      BorderStyle     =   0
      ForeColor       =   3355443
      HeadLines       =   2
      RowHeight       =   30
      RowDividerStyle =   6
      FormatLocked    =   -1  'True
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   6
      BeginProperty Column00 
         DataField       =   "member_id"
         Caption         =   "ID"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   "member_name"
         Caption         =   "Name"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column02 
         DataField       =   "address"
         Caption         =   "Address"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column03 
         DataField       =   "phone"
         Caption         =   "Phone"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column04 
         DataField       =   "register_date"
         Caption         =   "Register Date"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column05 
         DataField       =   "can_issue"
         Caption         =   "Issue Status"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         BeginProperty Column00 
            ColumnWidth     =   1094.74
         EndProperty
         BeginProperty Column01 
            DividerStyle    =   0
            ColumnWidth     =   2624.882
         EndProperty
         BeginProperty Column02 
            DividerStyle    =   0
            ColumnWidth     =   3135.118
         EndProperty
         BeginProperty Column03 
            DividerStyle    =   0
            ColumnWidth     =   2085.166
         EndProperty
         BeginProperty Column04 
            ColumnWidth     =   2085.166
         EndProperty
         BeginProperty Column05 
            ColumnWidth     =   870.236
         EndProperty
      EndProperty
   End
   Begin VB.Frame searchFrame 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H80000008&
      Height          =   1575
      Left            =   0
      TabIndex        =   13
      Top             =   0
      Width           =   9735
      Begin VB.ComboBox viewCombo 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         IntegralHeight  =   0   'False
         ItemData        =   "viewMemberEntry.frx":0015
         Left            =   6120
         List            =   "viewMemberEntry.frx":001F
         TabIndex        =   17
         Text            =   "Single"
         Top             =   720
         Width           =   2055
      End
      Begin VB.TextBox searchInput 
         Appearance      =   0  'Flat
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   360
         TabIndex        =   1
         Top             =   720
         Width           =   3375
      End
      Begin VB.CommandButton getMemberDetailBtn 
         Caption         =   "Get member detail"
         Height          =   375
         Left            =   3960
         TabIndex        =   2
         Top             =   720
         Width           =   1815
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "View"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   6120
         TabIndex        =   18
         Top             =   360
         Width           =   2055
      End
      Begin VB.Label searchInputLabel 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Member ID"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   360
         TabIndex        =   14
         Top             =   360
         Width           =   4215
      End
   End
   Begin VB.Frame detailFrame 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      Caption         =   "detailFrame"
      ForeColor       =   &H80000008&
      Height          =   5775
      Left            =   120
      TabIndex        =   7
      Top             =   1680
      Width           =   9615
      Begin VB.CommandButton returnBtn 
         Caption         =   "Return an Issued Book"
         Height          =   615
         Left            =   4320
         TabIndex        =   5
         Top             =   3960
         Visible         =   0   'False
         Width           =   2055
      End
      Begin VB.Frame warnInfo 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         ForeColor       =   &H80000008&
         Height          =   615
         Left            =   360
         TabIndex        =   8
         Top             =   2760
         Width           =   5775
         Begin VB.Label borrowedLabel 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "This member already has a book issued. "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00404040&
            Height          =   375
            Left            =   240
            TabIndex        =   9
            Top             =   120
            Width           =   5415
            WordWrap        =   -1  'True
         End
      End
      Begin VB.CommandButton editBtn 
         Caption         =   "Update Member detail"
         Height          =   615
         Left            =   240
         TabIndex        =   3
         Top             =   3960
         Width           =   2295
      End
      Begin VB.CommandButton delBtn 
         Caption         =   "Delete"
         Height          =   615
         Left            =   2640
         TabIndex        =   4
         Top             =   3960
         Width           =   1575
      End
      Begin VB.Label rdatelable 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "Registered on. 13/07/2010"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00808080&
         Height          =   255
         Left            =   360
         TabIndex        =   15
         Top             =   2400
         Width           =   3375
      End
      Begin VB.Label mname 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Name"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   495
         Left            =   360
         TabIndex        =   12
         Top             =   600
         Width           =   5895
      End
      Begin VB.Label phonelabel 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Ph. 7890721142"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00808080&
         Height          =   255
         Left            =   360
         TabIndex        =   11
         Top             =   1080
         Width           =   5895
      End
      Begin VB.Label addresslabel 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Address"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00808080&
         Height          =   255
         Left            =   360
         TabIndex        =   10
         Top             =   1680
         Width           =   6015
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00C0C0C0&
         X1              =   360
         X2              =   6120
         Y1              =   1560
         Y2              =   1560
      End
   End
   Begin VB.Frame msgFrame 
      Appearance      =   0  'Flat
      BackColor       =   &H000000C0&
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      ForeColor       =   &H80000008&
      Height          =   735
      Left            =   0
      TabIndex        =   0
      Top             =   1560
      Visible         =   0   'False
      Width           =   9735
      Begin VB.Timer msgHideTimer 
         Left            =   9120
         Top             =   240
      End
      Begin VB.Label msgLabel 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Error data shows here!"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   360
         TabIndex        =   6
         Top             =   240
         Width           =   1950
      End
   End
   Begin MSAdodcLib.Adodc Adodc1 
      Height          =   375
      Left            =   0
      Top             =   7560
      Visible         =   0   'False
      Width           =   2655
      _ExtentX        =   4683
      _ExtentY        =   661
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   2
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=database/LMSDB.accdb;Persist Security Info=False"
      OLEDBString     =   "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=database/LMSDB.accdb;Persist Security Info=False"
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   "members"
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
End
Attribute VB_Name = "viewMemberEntry"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim CanIssue As Boolean
Dim CurrentMemberId As String
Public UpdatedMemberId As String

Private Sub Form_Load()
    header.title.Caption = "Get Member detail"
    header.desc.Caption = "View member information using member ID"
    
    detailFrame.Visible = False
    msgFrame.Visible = False
    
    ResizeListGrid
    Form_Resize
    'this basically preload a view if the book is updated and sent here
    If UpdatedMemberId <> "" Then
        Adodc1.Refresh
        Adodc1.Recordset.Find "member_id=" + Str(Val(UpdatedMemberId))
        
        If (Adodc1.Recordset.EOF = False) Then
            'record found show, show it to user
            With Adodc1.Recordset
                createMemberDetailView .Fields(0), .Fields(1), .Fields(2), .Fields(3), .Fields(4), .Fields(5)
                searchInput.Text = UpdatedMemberId
                showMsg "This book has been updated!", Success
            End With
        Else
            'no record found! ;(
            showMsg "We couldn't found this book in our library!", Err
        End If
    End If
End Sub

Private Sub Form_Resize()
    searchFrame.Width = Me.Width
    msgFrame.Width = Me.Width
    ResizeListGrid
    
    If (viewCombo.Text = "List") Then
        ResizeListGrid
    End If
End Sub

Private Sub ResizeListGrid()
    DataGrid1.Width = Me.Width
    DataGrid1.Height = Me.Height - searchFrame.Height
End Sub

Private Sub Form_Unload(Cancel As Integer)
    CurrentMemberId = ""
    UpdatedMemberId = ""
End Sub

Private Sub getMemberDetailBtn_Click()
    Dim member_name As String
    
    'hide any previous error that is still showing!
    hideError
    
    'if no search string is provided then don't do anything
    If (searchInput.Text = "") Then
        showMsg "Searchbox is empty! please provide some input", Warning
        Exit Sub
    End If

    Adodc1.Refresh
    Adodc1.Recordset.Find "member_id=" + Str(Val(searchInput.Text))
    
    If (Adodc1.Recordset.EOF = False) Then
        'record found show, show it to user
        With Adodc1.Recordset
            createMemberDetailView .Fields(0), .Fields(1), .Fields(2), .Fields(3), .Fields(4), .Fields(5)
        End With
    Else
        'no record found! ;(
        showMsg "We couldn't found this Member!", Err
    End If
End Sub

Private Sub createMemberDetailView(member_id As String, member_name As String, address As String, phone As String, rdate As String, can_issue As String)
    CurrentMemberId = member_id
    mname.Caption = member_name
    phonelabel.Caption = "Ph. " + phone
    rdatelable.Caption = "Registered on. " + rdate
    addresslabel.Caption = address
    
    If (can_issue = "False") Then
        CanIssue = False
        warnInfo.Visible = True
        'returnBtn.Visible = True
        delBtn.Enabled = False
    Else
        CanIssue = True
        warnInfo.Visible = False
        'returnBtn.Visible = False
        delBtn.Enabled = True
    End If
    detailFrame.Visible = True
End Sub

Private Sub delBtn_Click()
    Dim delConfirm As VbMsgBoxResult
    delConfirm = MsgBox("Are you sure you want to delete this Member? This action is permanent!", vbYesNo, "Delete Book?")

    If delConfirm = vbYes Then
        On Error Resume Next
        Adodc1.Recordset.Delete
        CurrentMemberId = Null
        CanIssue = Null
        detailFrame.Visible = False
        showMsg "Member account is successfully removed!", Success
    End If
End Sub

Private Sub editBtn_Click()
    editMemberEntry.memberRecordIdtoModify = CurrentMemberId
    BaseForm.showForm editMemberEntry
End Sub

Private Sub showMsg(msg As String, Optional mtype As MessageType)
    Select Case mtype
        Case MessageType.Err
        msgFrame.BackColor = RGB(192, 0, 0)
        
        Case MessageType.Success
        msgFrame.BackColor = RGB(104, 188, 62)
        
        Case MessageType.Warning
        msgFrame.BackColor = RGB(243, 127, 9)
    End Select
    
    msgFrame.Visible = True
    msgLabel.Caption = msg
    
    msgHideTimer.Enabled = False
    msgHideTimer.Interval = 3000
    msgHideTimer.Enabled = True
    
    detailFrame.Top = 2400
End Sub

Private Sub hideError()
    msgFrame.Visible = False
    msgHideTimer.Enabled = False
    detailFrame.Top = 1680
End Sub

'hides the error after some defined time
Private Sub msgHideTimer_Timer()
    hideError
End Sub


Private Sub viewCombo_Click()
    If (viewCombo.Text = "Single") Then
        If (CurrentMemberId <> "") Then
            detailFrame.Visible = True
        End If
        DataGrid1.Visible = False
    ElseIf (viewCombo.Text = "List") Then
        detailFrame.Visible = False
        DataGrid1.Visible = True
        DataGrid1.Top = 1560
        DataGrid1.Left = 0
    End If
End Sub

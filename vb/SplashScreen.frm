VERSION 5.00
Begin VB.Form SplashScreen 
   BorderStyle     =   0  'None
   Caption         =   "Library Management System loading..."
   ClientHeight    =   5010
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   8460
   LinkTopic       =   "Form1"
   Picture         =   "SplashScreen.frx":0000
   ScaleHeight     =   5010
   ScaleWidth      =   8460
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Timer Timer1 
      Left            =   7920
      Top             =   3120
   End
End
Attribute VB_Name = "SplashScreen"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()
Timer1.Interval = 500
Timer1.Enabled = True
End Sub

Private Sub Timer1_Timer()
BaseForm.Show
Unload Me
End Sub

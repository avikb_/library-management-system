VERSION 5.00
Begin VB.Form Login 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BackColor       =   &H00404040&
   BorderStyle     =   0  'None
   Caption         =   "Admin Login"
   ClientHeight    =   8340
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   11595
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   Moveable        =   0   'False
   ScaleHeight     =   8340
   ScaleWidth      =   11595
   ShowInTaskbar   =   0   'False
   Begin VB.Frame loginFrame 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      Caption         =   "Login"
      ForeColor       =   &H80000008&
      Height          =   3495
      Left            =   3720
      TabIndex        =   0
      Top             =   2400
      Width           =   5415
      Begin VB.TextBox passwordTextbox 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         IMEMode         =   3  'DISABLE
         Left            =   360
         PasswordChar    =   "*"
         TabIndex        =   4
         Top             =   1680
         Width           =   4335
      End
      Begin VB.CommandButton loginBtn 
         Appearance      =   0  'Flat
         Caption         =   "Login"
         Height          =   495
         Left            =   360
         MaskColor       =   &H00000000&
         TabIndex        =   5
         Top             =   2400
         Width           =   1695
      End
      Begin VB.TextBox usernameTextbox 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   360
         TabIndex        =   3
         Top             =   600
         Width           =   4305
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Password"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   360
         TabIndex        =   2
         Top             =   1320
         Width           =   2055
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Username"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   360
         TabIndex        =   1
         Top             =   240
         Width           =   2055
      End
   End
   Begin VB.Image Image1 
      Height          =   8415
      Left            =   0
      Picture         =   "Login.frx":0000
      Stretch         =   -1  'True
      Top             =   0
      Width           =   11655
   End
End
Attribute VB_Name = "Login"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim password, username As String

Private Sub Form_Load()
    header.title.Caption = "Login"
    header.desc.Caption = "You need to login with your librarian account for using this software"
    Set BaseForm.currentActiveForm = Me
    
    username = "qw"
    password = "qw"
    sidebar.Enabled = False
End Sub

Private Sub Form_Resize()
Image1.Width = Me.Width
Image1.Height = Me.Height
loginFrame.Top = (Me.Height / 2) - loginFrame.Height / 2
loginFrame.Left = (Me.Width / 2) - loginFrame.Width / 2
End Sub

Private Sub loginBtn_Click()
    userValidation
End Sub

Private Sub userValidation()
    If usernameTextbox.Text = username And passwordTextbox.Text = password Then
        BaseForm.doLogin
    Else
        MsgBox "The username or the password you provided is not correct!", vbOKOnly, "Error, wrong credentials!"
    End If
End Sub

